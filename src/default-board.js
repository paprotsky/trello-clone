import { uuid } from './utils'

export default {
  name: 'workshop',
  columns: [
    {
      name: 'todo',
      tasks: [
        {
          description: '',
          name: 'task #1',
          id: uuid(),
          userAssigned: null
        },
        {
          description: '',
          name: 'task #2',
          id: uuid(),
          userAssigned: null
        },
        {
          description: '',
          name: 'task #3',
          id: uuid(),
          userAssigned: null
        }
      ]
    },
    {
      name: 'in-progress',
      tasks: [
        {
          description: '',
          name: 'task #4',
          id: uuid(),
          userAssigned: null
        }
      ]
    },
    {
      name: 'done',
      tasks: [
        {
          description: '',
          name: 'task #5',
          id: uuid(),
          userAssigned: null
        }
      ]
    }
  ]
}
