export default {
  props: {
    board: {
      type: Object,
      required: true
    },
    column: {
      type: Object,
      required: true
    },
    columnIndex: {
      type: Number,
      required: true
    }
  },

  methods: {
    moveColumn (e, toColumnIndex) {
      const fromColumnIndex = e.dataTransfer.getData('from-column-index')

      this.$store.commit('MOVE_COLUMN', {
        fromColumnIndex,
        toColumnIndex
      })
    },

    moveTaskOrColumn (e, toColumn, toColumnIndex, toTaskIndex) {
      const type = e.dataTransfer.getData('type')

      if (type === 'task') {
        this.moveTask(e, toColumn, toTaskIndex !== undefined ? toTaskIndex : toColumn.tasks.length)
      } else {
        this.moveColumn(e, toColumnIndex)
      }
    },

    moveTask (e, toColumn, toTaskIndex) {
      const fromColumnIndex = e.dataTransfer.getData('from-column-index')
      const fromColumn = this.board.columns[fromColumnIndex]

      const fromTaskIndex = e.dataTransfer.getData('from-task-index')

      this.$store.commit('MOVE_TASK', {
        fromColumn,
        toColumn,
        fromTaskIndex,
        toTaskIndex
      })
    }
  }
}
